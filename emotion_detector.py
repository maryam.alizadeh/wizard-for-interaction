import cv2
from deepface import DeepFace
import numpy as np
from threading import Thread, Lock

class EmotionDetector(Thread):
    def __init__(self, Xframe):
        super(EmotionDetector,self).__init__()
        self.face_cascade_name = cv2.data.haarcascades + 'haarcascade_frontalface_alt.xml' 
        self.face_cascade = cv2.CascadeClassifier()
        self.Xframe = Xframe
        if not self.face_cascade.load(cv2.samples.findFile(self.face_cascade_name)): 
            print("Error loading xml file")

        self.last_emotion = None       
        self.last_emotion_mutex = Lock()

        self.continue_capture = None
        self.capture_mutex = Lock()

    def run(self): # override 
        self.video=cv2.VideoCapture(0)
        self.fi = 0
        self.all_emos = {}
        self.continue_capture = True
        last_emotion = None
        while self.video.isOpened() and self.continue_capture:
            _,self.frame = self.video.read()
            self.gray=cv2.cvtColor(self.frame,cv2.COLOR_BGR2GRAY)
            self.face=self.face_cascade.detectMultiScale(self.gray,scaleFactor=1.1,minNeighbors=5)
            for x,y,w,h in self.face:
                self.img=cv2.rectangle(self.frame,(x,y),(x+w,y+h),(0,0,255),1)
            try:
                self.analyze = DeepFace.analyze(self.frame,actions=['emotion'])
                emo = str(self.analyze['dominant_emotion'])
                if emo not in self.all_emos:
                    self.all_emos[emo] = 0
                self.all_emos[emo] = self.all_emos[emo]+1
            except Exception as e:
                emo = str(e).split('.')[0]

            cv2.imshow('video', self.frame)
            self.key=cv2.waitKey(1)
            # if self.key==ord('q'):
            #     self.video.release()
            #     break            
            if self.fi % self.Xframe == 0:
                if len(self.all_emos) > 0:
                    last_emotion = max(self.all_emos, key=self.all_emos.get)                    
                else:
                    last_emotion = None
                self.set_last_emotion(last_emotion)
                self.all_emos = {}                
            print(f"FID: {self.fi}, Current Emotion: {emo}, Frequent Emotion: {last_emotion} (XFrame: {self.Xframe}), ")
            self.fi += 1
        self.video.release()

    def set_last_emotion(self, last_emotion):
        self.last_emotion_mutex.acquire()
        self.last_emotion = last_emotion
        self.last_emotion_mutex.release()

    def get_last_emotion(self):
        self.last_emotion_mutex.acquire()
        last_emotion = self.last_emotion
        self.last_emotion_mutex.release()        
        return last_emotion

    def set_xframe(self, xframe):
        self.xframe_mutex.acquire()
        self.Xframe = xframe
        self.xframe_mutex.release()

    def get_xframe(self, xframe):
        self.xframe_mutex.acquire()
        xframe = self.Xframe
        self.xframe_mutex.release()
        return xframe


    def stop(self):
        self.capture_mutex.acquire()
        self.continue_capture = False
        self.capture_mutex.release()        

