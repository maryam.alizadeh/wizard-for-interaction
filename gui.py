from tkinter import *
import subprocess
from FlokaROSExample import FlokaROS
import rospy
import time
from PIL import ImageTk, Image
from emotion_detector import EmotionDetector
import cv2
from deepface import DeepFace
import numpy as np
import threading
import time

from hlrc_server.msg import *

floka = FlokaROS("Tutorial")
class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("A simple Interaction GUI")

        self.greeting_button = Button(master, text="Greet", command=self.greet, width = 5)
        self.greeting_button.place(x=50, y=40)

        self.record_button = Button(master, text="start recording", command=self.record, width = 15)
        self.record_button.place(x=50, y=100)        

        self.stop_button = Button(master, text="stop recording", command=self.stop_recording, width = 15)
        self.stop_button.place(x=50, y=150)

        self.close_button = Button(master, text="Close", command=master.quit, width = 15)
        self.close_button.place(x=50, y=220)
        #self.close_button.pack()
        
        self.speak_button = Button(master, text="Speak to User", command=self.speak, width = 15)
        self.speak_button.place(x=50, y=280)
        #self.close_button.pack()

        xframe = 10
        self.recording = False
        self.xframe = xframe

    
    def record(self):
        if not self.recording:
            self.emotion_detector = EmotionDetector(self.xframe)
            self.emotion_detector.start()
            self.recording = True

    def stop_recording(self):
        if self.recording:
            self.emotion_detector.stop()
            self.recording = False

    def speak(self):
        last_emotion = self.emotion_detector.get_last_emotion()
        if last_emotion == "sad":
            say = floka.say_something("you look sad")
            
        elif last_emotion == "surprised":
            say = floka.say_something("you look surprised")
            
        elif last_emotion == "happy":
            say = floka.say_something("you look happy")
            
        elif last_emotion == "angry":
            say = floka.say_something("you look angry")
            
        elif last_emotion == "Neutral":
            say = floka.say_something("you look Neutral")
            
        else:
            say = floka.say_something("Emotion was not detected")
            
         
    
    def greet(self):
        say = floka.say_something("welcome to floka")
        #time.sleep(5.0)



root = Tk()
root.geometry("400x400")
#root["bg"] = "gray"
my_gui = MyFirstGUI(root)
root.mainloop()
my_gui.stop_recording()

