import rospy
import time

# Floka related imports to push messages in hlrc format
from hlrc_server.msg import *


class FlokaROS:
    def __init__(self, name):
        # Name ROS publisher on floka topics
        self.speechPublisher = rospy.Publisher('/floka/set/speech/goal', speechActionGoal, queue_size = 10)
        self.emotionPublisher = rospy.Publisher('/floka/set/currentEmotion/goal', emotionstateActionGoal, queue_size = 10)
        self.animationPublisher = rospy.Publisher('/floka/set/animation/goal', animationActionGoal, queue_size = 10)
        self.lookAtPublisher = rospy.Publisher('/floka/set/lookat/goal', lookattargetActionGoal, queue_size = 10)
        self.gazeAtPublisher = rospy.Publisher('/floka/set/gaze/goal', gazetargetActionGoal, queue_size = 10)

        # Initialize ROS node
        rospy.init_node(name, anonymous=True)
        rospy.loginfo("ROS-Node " + name + " initialize..")
        rospy.sleep(2.0) # a ros node need some time to register on master

    def say_something(self, message):
        """
            Request the robot to say something using tts
            :param message: text to synthesize
        """
        speechGoal = speechActionGoal()
        speechGoal.goal.text = message
        self.speechPublisher.publish(speechGoal)

    def set_emotion(self, emotionValue, duration):
        """
            Set the current emotion
            :param emotionValue: a RobotEmotion object
            :param duration: duration time in milliseconds
        """
        # Available Emotions:
        # RobotEmotion.ANGRY,
        # RobotEmotion.NEUTRAL,
        # RobotEmotion.HAPPY,
        # RobotEmotion.SAD,
        # RobotEmotion.SURPRISED,
        # RobotEmotion.FEAR
        emotionGoal = emotionstateActionGoal()
        emotionGoal.goal.value = emotionValue
        emotionGoal.goal.duration = duration
        self.emotionPublisher.publish(emotionGoal)

    def set_animation(self, animationValue, duration, repetition, scale):
        """
            Set a head animation
            :param animationValue: a available predefined robot animation
            :param duration: duration time in milliseconds for each repetition
            :param repetition: number of repetitions
            :param scale: number for scaling the animation
        """
        # Available Animations
        # IDLE  = 0
        # HEAD_NOD   = 1
        # HEAD_SHAKE = 2
        # EYEBLINK_L = 3
        # EYEBLINK_R = 4
        # EYEBLINK_BOTH  = 5
        # EYEBROWS_RAISE = 6
        # EYEBROWS_LOWER = 7
        # ENGAGEMENT_LEFT = 8
        # ENGAGEMENT_RIGHT = 9
        animationGoal = animationActionGoal()
        animationGoal.goal.target = animationValue
        animationGoal.goal.repetitions = repetition
        animationGoal.goal.duration_each = duration
        animationGoal.goal.scale = scale
        self.animationPublisher.publish(animationGoal)

    def look_at_something(self, x, y, z, roll):
        """
            Sets a gaze at a Cartesian position
            :param x,y,z: position to look at (in eyes frame) (head movement)
            :param roll: side-ways motion of head
        """
        lookatTargetGoal = lookattargetActionGoal()
        lookatTargetGoal.goal.point.point.x = x
        lookatTargetGoal.goal.point.point.y = y
        lookatTargetGoal.goal.point.point.z = z
        lookatTargetGoal.goal.roll = roll
        self.lookAtPublisher.publish(lookatTargetGoal)

    def gaze_at_something(self, pan, tilt, roll, vergence, pan_offset, tilt_offset, gazeType):
        """
            Sets a gaze at a Cartesian position (eye movement only)
        """
        gazeatTargetGoal = gazetargetActionGoal()
        gazeatTargetGoal.goal.pan = pan
        gazeatTargetGoal.goal.tilt = tilt
        gazeatTargetGoal.goal.roll = roll
        gazeatTargetGoal.goal.vergence = vergence
        gazeatTargetGoal.goal.pan_offset = pan_offset
        gazeatTargetGoal.goal.tilt_offset = tilt_offset
        gazeatTargetGoal.goal.gaze_type = gazeType
        self.gazeAtPublisher.publish(gazeatTargetGoal)

    def reset(self):
        """
            Reset Robot movements to default
        """
        self.gaze_at_something(0, 0, 0, 0, 0, 0, 0)


if __name__ == '__main__':
    try:
        floka = FlokaROS("Tutorial")
        floka.reset()
        time.sleep(2.0)

        rospy.loginfo("Speech")
        floka.say_something("Hallo Welt, ich bin Floka")
        time.sleep(5.0)
        
        rospy.loginfo("Emotion")
        floka.set_emotion(1, 1000)
        time.sleep(4.0)

        rospy.loginfo("Animation")
        floka.set_animation(3, 2000, 2, 0.5)
        time.sleep(7.0)

        rospy.loginfo("LookAt")
        floka.look_at_something(10, 0, 0, 0.5)
        time.sleep(5.0)

        rospy.loginfo("Reset")
        floka.reset()
        time.sleep(4.0)

        rospy.loginfo("GazeAt")
        floka.gaze_at_something(8, 4, 0.3, 0, 0, 0, 0)
        time.sleep(5.0)

        rospy.loginfo("Reset")
        floka.reset()
        time.sleep(2.0)

        rospy.loginfo("Done")

    except rospy.ROSInterruptException:
        pass