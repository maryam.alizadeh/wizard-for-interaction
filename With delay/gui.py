from tkinter import *
import subprocess
from FlokaROSExample import FlokaROS
import rospy
import time
from PIL import ImageTk, Image
from emotion_detector import EmotionDetector
#from emotion2edited import Emotion 
import cv2
from deepface import DeepFace
import numpy as np
import threading
import time

from hlrc_server.msg import *

floka = FlokaROS("Tutorial")
#emotion = Emotion(10)
#emotion = Emotion(10)  
class MyFirstGUI:
    def __init__(self, master):
        self.master = master
        master.title("A simple Interaction GUI")

        self.greeting_button = Button(master, text="Greet", command=self.greet, width = 5)
        self.greeting_button.place(x=50, y=40)
        ## for keyboard 
        master.bind('g', self.greet)

        
        self.record_button = Button(master, text="Record face", command=self.record, width = 15)
        self.record_button.place(x=50, y=100)        
        master.bind('r', self.record) 


        self.stop_button = Button(master, text="End recording", command=self.stop_recording, width = 15)
        self.stop_button.place(x=50, y=150)
        master.bind('e', self.stop_recording)

        self.close_button = Button(master, text="Quit the gui", command=master.quit, width = 15)
        self.close_button.place(x=50, y=220)
        master.bind('q', master.quit)
        #self.close_button.pack()
        
        self.speak_button = Button(master, text="Talk to User", command=self.speak, width = 15)
        self.speak_button.place(x=50, y=280)
        master.bind('t', self.speak)
        #self.close_button.pack()

        xframe = 10
        self.recording = False
        self.xframe = xframe

    ## 
    def record(self, _event=None):
        if not self.recording:
            self.emotion_detector = EmotionDetector(self.xframe)
            self.emotion_detector.start()
            self.recording = True

    def stop_recording(self, _event=None):
        if self.recording:
            self.emotion_detector.stop()
            self.recording = False
            

    def speak(self, _event=None):
        last_emotion = self.emotion_detector.get_last_emotion()
        if last_emotion == "sad":
            say = floka.say_something("you look sad, please be a bit happy")
            #print("\nyou look sad\n")
        elif last_emotion == "surprise":
            say = floka.say_something("you look surprised")
            #print("\nyou look surprised\n")
        elif last_emotion == "happy":
            say = floka.say_something("you look happy, it is great")
            #print("\nyou look happy\n")
        elif last_emotion == "angry":
            say = floka.say_something("you look angry, please calm down?")
            #print("\nyou look angry\n")
        elif last_emotion == "fear":
            say = floka.say_something("you look feared")
            #print("\nyou look angry\n")
        elif last_emotion == "neutral":
            say = floka.say_something("you look Neutral")
            #print("\nyou look Neutral\n")
        else:
            say = floka.say_something("Emotion unknown")
            print(last_emotion)
            #print("\nyou look None\n")
         
    
    def greet(self, _event=None):
        say = floka.say_something("Herzlich willkommen!")
        #time.sleep(2.0)
        say = floka.say_something("Vielen Dank, dass Sie an einem Experiment zur Erforschung von Emotionen im TRR 318 Projekt Constructing Explainability teilnehmen.")
        time.sleep(3.0)
        say = floka.say_something("Die Studie wird ungefähr 20 Minuten dauern.")
        time.sleep(3.0)
        say = floka.say_something("Hinweise zur Datensicherheit: Die erhobenen Daten dienen ausschließlich wissenschaftlichen Zwecken und werden selbstverständlich streng vertraulich behandelt.")
        time.sleep(3.0)
        say = floka.say_something("Die Teilnahme erfolgt anonym und ohne Rückschlüsse auf einzelne Personen.")
        
        #time.sleep(5.0)



root = Tk()
root.geometry("400x400")
#root["bg"] = "gray"
my_gui = MyFirstGUI(root)
root.mainloop()
my_gui.stop_recording()

