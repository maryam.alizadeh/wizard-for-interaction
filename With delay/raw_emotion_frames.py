import cv2
from deepface import DeepFace
import numpy as np
face_cascade_name = cv2.data.haarcascades + 'haarcascade_frontalface_alt.xml' 
face_cascade = cv2.CascadeClassifier()
if not face_cascade.load(cv2.samples.findFile(face_cascade_name)): 
    print("Error loading xml file")
video=cv2.VideoCapture(0) 

# all emos
fi = 0
all_emos = {}

while True:
    try:
        Xframe = int(input('Enter the number of frames: '));
        break
    except ValueError:
        print("\tError: Input vlaue must be an integer number!")
        pass

while video.isOpened(): 
    _,frame = video.read()
    gray=cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY) 
    face=face_cascade.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=5)
    for x,y,w,h in face:
        img=cv2.rectangle(frame,(x,y),(x+w,y+h),(0,0,255),1) 
    try:
        analyze = DeepFace.analyze(frame,actions=['emotion'])
        emo = str(analyze['dominant_emotion'])
        print(emo)
        if emo not in all_emos:
            all_emos[emo] = 0
        all_emos[emo] = all_emos[emo]+1
    except Exception as e:
        print("")

    cv2.imshow('video', frame)
    key=cv2.waitKey(1)
    if key==ord('q'):    
        video.release()
        break

    print(f"fi = {fi}")
    if fi % Xframe == 0:
        if len(all_emos) > 0:
            maxemo = max(all_emos, key=all_emos.get)
        else:
            maxemo = None
        print(f"Most frequent emotion in past {Xframe} frames: {maxemo}")
        print(all_emos)
        all_emos = {}
    
    fi += 1
